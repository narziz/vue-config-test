// const path = require("path");
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
      },
      {
        test: /\.css$/,
        use: ["css-loader"],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ["vue-style-loader", "css-loader", "sass-loader"],
      },
      {
        // image loader
        test: /\.(jpg|jpeg|png|gif)$/,
        type: 'asset/resource',
        generator: {
         filename: 'images/[name][ext][query]'
        },
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
  ],
};
