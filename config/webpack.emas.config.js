const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require('webpack-merge');

const common = require('../webpack.common');

module.exports = merge(common, {
  mode: 'development',
  entry: {
    index: './src/emas/main.js'
  },
  output: {
    filename: './js/main.js',
    path: path.resolve(__dirname, '../emas'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: 'Emas App',
      filename: 'index.html',
      template: './public/index.html',
      inject: 'body'
    })
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, '../emas'),
    },
    port: 4000,
  },
});