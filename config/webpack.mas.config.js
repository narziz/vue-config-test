const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require('webpack-merge');

const common = require('../webpack.common');

module.exports = merge(common, {
  mode: 'development',
  entry: {
    index: './src/mas/main.js'
  },
  output: {
    filename: './js/main.js',
    path: path.resolve(__dirname, '../mas'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: 'Mas App',
      filename: 'index.html',
      template: './public/index.html',
      inject: 'body'
    })
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, '../mas'),
    },
    port: 4000,
  },
});