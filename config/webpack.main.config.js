const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require('webpack-merge');

const common = require('../webpack.common');

module.exports = merge(common, {
  mode: "development",
  entry: {
    index: "./src/main.js",
  },
  output: {
    filename: "./js/main.js",
    path: path.resolve(__dirname, "../dist"),
  },
  plugins: [
    new HtmlWebpackPlugin({
      name: "Main App",
      filename: "index.html",
      template: "./public/index.html",
      inject: "body",
    }),
  ],
  devServer: {
    static: {
      directory: path.join(__dirname, "../dist"),
    },
    port: 4000,
  },
})